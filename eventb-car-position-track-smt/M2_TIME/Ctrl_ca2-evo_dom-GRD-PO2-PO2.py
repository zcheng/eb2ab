
from z3 import *

# decl

now = Real('now')
deltaT = Real('deltaT')
A = Real('A')
B = Real('B')
S = Real('S')
t = Real('t')
ta = Real('ta')
P = Array('P', RealSort(), RealSort())
V = Array('V', RealSort(), RealSort())
Pa = Array('Pa', RealSort(), RealSort())
Va = Array('Va', RealSort(), RealSort())

# CONTEXT
C00 = S > 0
C01 = B > 0
C02 = A > 0
C03 = deltaT > 0

# MACHINE
M01 = P[now] + (V[now] * V[now]) / (2*B) <= S   # INV

# Hypothesis
u = -B
H01 = And(now<t, t<=now+deltaT)                # GRD
#H02 = V[now] >= 0
#H03 = now >= 0
H04 = deltaT > V[now]/B
H05 = Implies(And(now<=t, t<=now+V[now]/B), Pa[t] == P[now]+V[now]*(t-now)+1/2*u*(t-now)*(t-now))
H05_ = Implies(And(now+V[now]/B<t, t<=now+deltaT), Pa[t] == 0)
H06 = Implies(And(now<=t, t<=now+V[now]/B), Va[t] == V[now]+u*(t-now))
H06_ = Implies(And(now+V[now]/B<t, t<=now+deltaT), Va[t] == 0)

# conclusion

c = Pa[t] + (Va[t] * Va[t]) / (2*B) <= S

# solve
s = Solver()
s.add(C00,C01,C02,C03,M01,H01,H04,H05,H05_,H06,H06_,Not(c))

if s.check() == sat:
    print(f"static checking model =  {s.model()}")
else:
    print(f"pass")
