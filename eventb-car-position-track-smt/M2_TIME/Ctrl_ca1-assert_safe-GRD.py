
from z3 import *

# decl

now = Real('now')
deltaT = Real('deltaT')
A = Real('A')
B = Real('B')
S = Real('S')
#t = Real('t')
P = Array('P', RealSort(), RealSort())
V = Array('V', RealSort(), RealSort())

# hypothesis

h1 = S > 0
h2 = B > 0
h3 = A > 0
h4 = deltaT > 0
h5 = P[now] + (V[now] * V[now]) / (2*B) + (A/B+1)*(A/2*deltaT*deltaT+deltaT*V[now]) <= S
#h6 = P[now] + (V[now] * V[now]) / (2*B) <= S
h7 = V[now] >= 0

# conclusion

c = P[now] + (V[now] * V[now]) / (2*B) < S

# solve
s = Solver()
s.add(h1,h2,h3,h4,h5,h7,Not(c))

if s.check() == sat:
    print(f"static checking model =  {s.model()}")
else:
    print(f"pass")
