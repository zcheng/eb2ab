
from z3 import *

# decl

now = Real('now')
deltaT = Real('deltaT')
A = Real('A')
B = Real('B')
S = Real('S')
t = Real('t')

P = Array('P', RealSort(), RealSort())
V = Array('V', RealSort(), RealSort())
Pa = Array('Pa', RealSort(), RealSort())
Va = Array('Va', RealSort(), RealSort())

# hypothesis

h00 = S > 0
h01 = B > 0
h02 = A > 0
h03 = deltaT > 0

u = -B

h04 = And(now<t, t<=now+deltaT)
h05 = now >= 0
h06 = V[now] >= 0
h07 = deltaT <= V[now]/B
h08 = Va[t] == V[now]+u*(t-now)

# conclusion

c = Va[t] >= 0

# solve
s = Solver()
s.add(h00,h01,h02,h03,h04,h05,h06,h07,h08,Not(c))

if s.check() == sat:
    print(f"static checking model =  {s.model()}")
else:
    print(f"pass")
