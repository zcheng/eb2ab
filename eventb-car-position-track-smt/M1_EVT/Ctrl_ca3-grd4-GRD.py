
from z3 import *

# decl

ae = Real('ae') #p
ae0 = Real('ae0') #v

B = Real('B')
S = Real('S')

# Context
C01 = S > 0
C02 = B > 0

# Hypothesis
H01 = ae + (ae0 * ae0) / (2*B) <= S

# conclusion

c = ae <= S

# solve
s = Solver()
s.add(C01,C02,H01,Not(c))

if s.check() == sat:
    print(f"static checking model =  {s.model()}")
else:
    print(f"pass")
