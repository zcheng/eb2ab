
from z3 import *

# Decls
now = Real('now')
tx = Real('tx')
ta = Real('ta')

t0 = Real('t0')
p0 = Real('p0')
v0 = Real('v0')

B = Real('B')
S = Real('S')

t = Real('t')
P = Array('P', RealSort(), RealSort())
V = Array('V', RealSort(), RealSort())
Pa = Array('Pa', RealSort(), RealSort())
Va = Array('Va', RealSort(), RealSort())

# Context axioms
C01 = S > 0
C02 = B > 0
C03 = p0 <= S
C04 = t0 >= 0
C05 = v0 >= 0

# Machine invariants
M01 = And(is_real(now), 0<=t0, t0<=now)
M02 = Implies(And(t0<=t, t<=now), P[t]+((V[t]*V[t])/(2*B)) <= S)

# Hypothesis
H01 = Implies(And(now<t, t<=now+ta), Pa[t]+((Va[t]*Va[t])/(2*B)) <= S)
H02 = Implies(And(now<t, t<=now+ta), Pa[t] == P[t])
H03 = Implies(And(now<t, t<=now+ta), Va[t] == V[t])

# Conclusion
c = Implies(And(t0<=t, t<=now+ta), P[t]+((V[t]*V[t])/(2*B)) <= S)

s = Solver()
s.add(C01,C02,C03,C04,C05,M01,M02,H01,H02,H03,Not(c))

if s.check() == sat:
    print(f"static checking model =  {s.model()}")
else:
    print(f"pass")
