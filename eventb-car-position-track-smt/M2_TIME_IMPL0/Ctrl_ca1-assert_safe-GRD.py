from z3 import *

# decl

deltaT = Real('deltaT')
A = Real('A')
B = Real('B')
S = Real('S')
dT = Int('dT')
Ai = Int('Ai')
Bi = Int('Bi')
Bi2 = Int('Bi2')
Si = Int('Si')

Ps = Real('Ps')
Vs = Real('Vs')
DPs = Int('DPs')    # DPs = adc(Ps) & Ps <= INR(DPs)
DVs = Int('DVs')

C1 = Int('C1')

# hypothesis

h1 = S > 0
h2 = B > 0
h3 = A > 0
h4 = deltaT > 0
h5 = Ps <= DPs
h6 = Vs <= DVs
h7 = 0 <= Vs        # inv1
h8_ = 0 <= Ps       # inv2
h9_ = B <= Bi       # theory car_ode_int
h9__ = And(0<=Bi2, Bi2 <= B)    # theory car_ode_int
h10_ = And(0<=Si, Si <= S)  # theory car_ode_int
h11_ = A <= Ai      # theory car_ode_int
h12_ = deltaT <= dT # theory car_ode_int
h13 = DPs*2*Bi + (DVs * DVs) + (Ai+Bi)*(Ai*dT*dT) + (Ai+Bi)*(2*dT*DVs) <= Si*2*Bi2 # proposed guard

# conclusion

# lemmas
# each of them can be proved by using above hypothesis
# since they are proved, they are added to hypothesis list
c1 = Ps*2*B <= DPs*2*Bi
c2 = (Vs * Vs) <= (DVs * DVs)
c3 = (A+B)*(A*deltaT*deltaT) <= (Ai+Bi)*(Ai*dT*dT) # SLOW
c4 = (A+B)*(2*deltaT*Vs) <= (Ai+Bi)*(2*dT*DVs)
c5 = Ps*2*B+(Vs*Vs)+(A+B)*(A*deltaT*deltaT)+(A+B)*(2*deltaT*Vs) <= DPs*2*Bi + (DVs * DVs) + (Ai+Bi)*(Ai*dT*dT) + (Ai+Bi)*(2*dT*DVs) # by plus compatible of c1 c2 c3 c4
c6 = Si*2*Bi2 <= S*2*B

# conclusion
c = Ps*2*B + (Vs*Vs) + (A+B)*(A*deltaT*deltaT+2*deltaT*Vs) <= S*2*B

# solve
s = Solver()
s.add(h1,h2,h3,h4,h5,h6,h7,h8_,h9_,h9__,h10_,h11_,h12_,h13,c1,c2,c3,c4,c5,c6,Not(c))

if s.check() == sat:
    print(f"static checking model =  {s.model()}")
else:
    print(f"pass")
