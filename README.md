# From System Events to Software Operations for Refinement-based Modeling of Hybrid Systems

## Abstract

To reduce error-prone work of implementing hybrid system designs in Event-B by hand, we revisit the refinement methodology to systematically identify, categorize and modularize software operators from system events. Then, by defining a verified translation, we can obtain a semantically equivalent correspondent in the B language for each modularized Event-B software operator.
Thus, we can reuse the primitives of B (which are superset of Event-B) for refining system events down to implementations, and reuse the predicate transformers defined on the B primitives to reason about the correctness of refinements. The verified translation also ensures that the behaviors of the implementations obtained via refinements in B do not divert from the corresponding system events specified in Event-B. This repository contains the evaluation of our proposal on two case studies.

## Requirements

* Rodin (3.6.0-77c344946)
  * Event-B Theory Feature (4.0.4.202204011240-f60c57a0)
  * SMT Solvers	(1.5.0.c5fa8c25)
  * Atelier B provers (2.3.0.r16736)
* Dafny (v2.9.0)
* Atelier-B (v4.7.1)

## Reproduce

Under the given requirements

### Event-B

* The fully developed Event-B models can be viewed and reproved in the Rodin platform.

### Dafny

* The certified translation from a Event-B specification to its B specification can be viewed and reproved in the Dafny automatic prover.

### Atelier-B

* The fully developed B implementation can be viewed and reproved in the Atelier-B platform.
  * download the B archive from this repository
  * Create a new workspace in Atelier-B (Workspaces -> New -> Workspace ... -> give it a new workspace name)
  * right click newly created workspace -> Restore project -> Next
    * workspace, select the newly created workspace.
    * archive, select the downloaded B archive.
    * destination directory, select an *empty* space that want to store the unzip archive.
    * click next and the archived B model is imported.


