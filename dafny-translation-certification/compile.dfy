
/* a simiple context store variable value pairs */

datatype value =
 | vint(i: int)
 | vreal(r: real)

type Context = map<string, value>

/*****************
 * event b model 
 *****************/

/* Integer */

datatype ebIntUnOp =
  | ebIntNeg

datatype ebIntBinOp = 
  | ebIntAdd
  | ebIntMul
  | ebIntSub
  | ebIntDiv

datatype ebIntRelOp = 
  | ebIntEq
  | ebIntNeq
  | ebIntLe
  | ebIntLt
  | ebIntGt
  | ebIntGe

datatype ebIntExpr =
  | ebIntLitExpr(n: int) 
  | ebIntIdExpr(vi: string)
  | ebIntUnExpr(uop: ebIntUnOp, e: ebIntExpr)
  | ebIntBinExpr(op: ebIntBinOp, e1: ebIntExpr, e2: ebIntExpr)

/* Real */

datatype ebRealUnOp =
  | ebRealNeg

datatype ebRealBinOp = 
  | ebRealAdd
  | ebRealMul
  | ebRealSub
  | ebRealDiv

datatype ebRealRelOp = 
  | ebRealEq
  | ebRealNeq
  | ebRealLe
  | ebRealLt
  | ebRealGt
  | ebRealGe

datatype ebRealExpr =
  | ebRealLitZero
  | ebRealLitOne
  | ebRealIdExpr(vr: string)
  | ebRealUnExpr(uop: ebRealUnOp, e: ebRealExpr)
  | ebRealBinExpr(op: ebRealBinOp, e1: ebRealExpr, e2: ebRealExpr)

/* Predicate */  

datatype ebBinPredOp =
  | ebLand
  | ebLor
  | ebImply
  | ebEquiv

datatype ebUnPredOp =
  | ebNot

datatype ebLitPredVal =
  | ebTrue
  | ebFalse

datatype ebPred = 
  | ebLitPred(v: ebLitPredVal)
  | ebUnPred(op: ebUnPredOp, p: ebPred)
  | ebBinPred(bop: ebBinPredOp, p1: ebPred, p2: ebPred)
  | ebIntRelPred(riop: ebIntRelOp, rip1: ebIntExpr, rip2: ebIntExpr)
  | ebRealRelPred(rrop: ebRealRelOp, rrp1: ebRealExpr, rrp2: ebRealExpr)

/* interpret eventb int expression in a context */

function method interp_ebIntExpr(e: ebIntExpr, ctx: Context): int 
decreases e
{
  match e {
    case ebIntLitExpr(n) => n
    case ebIntIdExpr(vi) => 
      if vi in ctx.Keys then 
        if ctx[vi].vint? then ctx[vi].i else 0
      else 0 
    case ebIntUnExpr(ebIntNeg, e) => - interp_ebIntExpr(e, ctx)
    case ebIntBinExpr(ebIntAdd, e1, e2) =>
      interp_ebIntExpr(e1, ctx) + interp_ebIntExpr(e2, ctx)
    case ebIntBinExpr(ebIntSub, e1, e2) =>
      interp_ebIntExpr(e1, ctx) - interp_ebIntExpr(e2, ctx)
    case ebIntBinExpr(ebIntMul, e1, e2) =>
      interp_ebIntExpr(e1, ctx) * interp_ebIntExpr(e2, ctx)
    case ebIntBinExpr(ebIntDiv, e1, e2) =>
      if interp_ebIntExpr(e2, ctx) != 0 then interp_ebIntExpr(e1, ctx) / interp_ebIntExpr(e2, ctx) else 0
  }
}

/* interpret eventb int expression in a context */

function method interp_ebRealExpr(e: ebRealExpr, ctx: Context): real 
decreases e
{
  match e {
    case ebRealLitZero => 0.0
    case ebRealLitOne => 1.0
    case ebRealIdExpr(vr) => 
      if vr in ctx.Keys then 
        if ctx[vr].vreal? then ctx[vr].r else 0.0
      else 0.0
    case ebRealUnExpr(ebRealNeg, e) => - interp_ebRealExpr(e, ctx)
    case ebRealBinExpr(ebRealAdd, e1, e2) =>
      interp_ebRealExpr(e1, ctx) + interp_ebRealExpr(e2, ctx)
    case ebRealBinExpr(ebRealSub, e1, e2) =>
      interp_ebRealExpr(e1, ctx) - interp_ebRealExpr(e2, ctx)
    case ebRealBinExpr(ebRealMul, e1, e2) =>
      interp_ebRealExpr(e1, ctx) * interp_ebRealExpr(e2, ctx)
    case ebRealBinExpr(ebRealDiv, e1, e2) =>
      if interp_ebRealExpr(e2, ctx) != 0.0 then 
        interp_ebRealExpr(e1, ctx) / interp_ebRealExpr(e2, ctx) 
      else 0.0
  }
}

/* interpret eventb predicate in a context */

predicate method interp_ebPred(p: ebPred, ctx: Context) 
decreases p
{
  match p {
    case ebLitPred(ebTrue) => true
    case ebLitPred(ebFalse) => false
    case ebUnPred(ebNot, p1) => ! interp_ebPred(p1, ctx)
    case ebBinPred(ebLand, p1, p2) => interp_ebPred(p1, ctx) && interp_ebPred(p2, ctx)
    case ebBinPred(ebLor, p1, p2) => interp_ebPred(p1, ctx) || interp_ebPred(p2, ctx)
    case ebBinPred(ebImply, p1, p2) => interp_ebPred(p1, ctx) ==> interp_ebPred(p2, ctx)
    case ebBinPred(ebEquiv, p1, p2) => interp_ebPred(p1, ctx) <==> interp_ebPred(p2, ctx)
    case ebIntRelPred(ebIntEq, e1, e2) => interp_ebIntExpr(e1, ctx) == interp_ebIntExpr(e2, ctx)
    case ebIntRelPred(ebIntNeq, e1, e2) => interp_ebIntExpr(e1, ctx) != interp_ebIntExpr(e2, ctx)
    case ebIntRelPred(ebIntLe, e1, e2) => interp_ebIntExpr(e1, ctx) <= interp_ebIntExpr(e2, ctx)
    case ebIntRelPred(ebIntLt, e1, e2) => interp_ebIntExpr(e1, ctx) < interp_ebIntExpr(e2, ctx)
    case ebIntRelPred(ebIntGe, e1, e2) => interp_ebIntExpr(e1, ctx) >= interp_ebIntExpr(e2, ctx)
    case ebIntRelPred(ebIntGt, e1, e2) => 
    interp_ebIntExpr(e1, ctx) > interp_ebIntExpr(e2, ctx)
    case ebRealRelPred(ebRealEq, e1, e2) => 
    interp_ebRealExpr(e1, ctx) == interp_ebRealExpr(e2, ctx)
    case ebRealRelPred(ebRealNeq, e1, e2) => 
    interp_ebRealExpr(e1, ctx) != interp_ebRealExpr(e2, ctx)
    case ebRealRelPred(ebRealLe, e1, e2) => 
    interp_ebRealExpr(e1, ctx) <= interp_ebRealExpr(e2, ctx)
    case ebRealRelPred(ebRealLt, e1, e2) => 
    interp_ebRealExpr(e1, ctx) < interp_ebRealExpr(e2, ctx)
    case ebRealRelPred(ebRealGe, e1, e2) => 
    interp_ebRealExpr(e1, ctx) >= interp_ebRealExpr(e2, ctx)
    case ebRealRelPred(ebRealGt, e1, e2) => 
    interp_ebRealExpr(e1, ctx) > interp_ebRealExpr(e2, ctx)
  }
}


/*****************
 * atelier b model 
 *****************/

/* Integer */

datatype abIntUnOp =
  | abIntNeg

datatype abIntBinOp = 
  | abIntAdd
  | abIntMul
  | abIntSub
  | abIntDiv

datatype abIntRelOp = 
  | abIntEq
  | abIntNeq
  | abIntLe
  | abIntLt
  | abIntGt
  | abIntGe

datatype abIntExpr =
  /* Integer Lit Expr */    | abIntLitExpr(n: int) 
  /* Identifer Expr */      | abIntIdExpr(vi: string)
  /* Unary Int Expr */      | abIntUnExpr(uop: abIntUnOp, e: abIntExpr)
  /* Binary Int Expr */     | abIntBinExpr(op: abIntBinOp, e1: abIntExpr, e2: abIntExpr)

/* Real */

datatype abRealUnOp =
  | abRealNeg

datatype abRealBinOp = 
  | abRealAdd
  | abRealMul
  | abRealSub
  | abRealDiv

datatype abRealRelOp = 
  | abRealEq
  | abRealNeq
  | abRealLe
  | abRealLt
  | abRealGt
  | abRealGe

datatype abRealExpr =
  | abRealLitZero
  | abRealLitOne
  | abRealIdExpr(vr: string)
  | abRealUnExpr(uop: abRealUnOp, e: abRealExpr)
  | abRealBinExpr(op: abRealBinOp, e1: abRealExpr, e2: abRealExpr)

/* Predicate */  

datatype abBinPredOp =
  | abLand
  | abLor
  | abImply
  | abEquiv

datatype abUnPredOp =
  | abNot

datatype abLitPredVal =
  | abTrue
  | abFalse

datatype abPred = 
  | abLitPred(v: abLitPredVal)
  | abUnPred(op: abUnPredOp, p: abPred)
  | abBinPred(bop: abBinPredOp, p1: abPred, p2: abPred)
  | abIntRelPred(riop: abIntRelOp, rip1: abIntExpr, rip2: abIntExpr)
  | abRealRelPred(rrop: abRealRelOp, rrp1: abRealExpr, rrp2: abRealExpr)

/* interpret eventb int expression in a context */

function method interp_abIntExpr(e: abIntExpr, ctx: Context): int 
decreases e
{
  match e {
    case abIntLitExpr(n) => n
    case abIntIdExpr(vi) => 
      if vi in ctx.Keys then 
        if ctx[vi].vint? then ctx[vi].i else 0
      else 0 
    case abIntUnExpr(abIntNeg, e) => - interp_abIntExpr(e, ctx)
    case abIntBinExpr(abIntAdd, e1, e2) =>
      interp_abIntExpr(e1, ctx) + interp_abIntExpr(e2, ctx)
    case abIntBinExpr(abIntSub, e1, e2) =>
      interp_abIntExpr(e1, ctx) - interp_abIntExpr(e2, ctx)
    case abIntBinExpr(abIntMul, e1, e2) =>
      interp_abIntExpr(e1, ctx) * interp_abIntExpr(e2, ctx)
    case abIntBinExpr(abIntDiv, e1, e2) =>
      if interp_abIntExpr(e2, ctx) != 0 then interp_abIntExpr(e1, ctx) / interp_abIntExpr(e2, ctx) else 0
  }
}

/* interpret eventb int expression in a context */

function method interp_abRealExpr(e: abRealExpr, ctx: Context): real 
decreases e
{
  match e {
    case abRealLitZero => 0.0
    case abRealLitOne => 1.0
    case abRealIdExpr(vr) => 
      if vr in ctx.Keys then 
        if ctx[vr].vreal? then ctx[vr].r else 0.0
      else 0.0
    case abRealUnExpr(abRealNeg, e) => - interp_abRealExpr(e, ctx)
    case abRealBinExpr(abRealAdd, e1, e2) =>
      interp_abRealExpr(e1, ctx) + interp_abRealExpr(e2, ctx)
    case abRealBinExpr(abRealSub, e1, e2) =>
      interp_abRealExpr(e1, ctx) - interp_abRealExpr(e2, ctx)
    case abRealBinExpr(abRealMul, e1, e2) =>
      interp_abRealExpr(e1, ctx) * interp_abRealExpr(e2, ctx)
    case abRealBinExpr(abRealDiv, e1, e2) =>
      if interp_abRealExpr(e2, ctx) != 0.0 then 
        interp_abRealExpr(e1, ctx) / interp_abRealExpr(e2, ctx) 
      else 0.0
  }
}

/* interpret eventb predicate in a context */

predicate method interp_abPred(p: abPred, ctx: Context) 
decreases p
{
  match p {
    case abLitPred(abTrue) => true
    case abLitPred(abFalse) => false
    case abUnPred(abNot, p1) => ! interp_abPred(p1, ctx)
    case abBinPred(abLand, p1, p2) => interp_abPred(p1, ctx) && interp_abPred(p2, ctx)
    case abBinPred(abLor, p1, p2) => interp_abPred(p1, ctx) || interp_abPred(p2, ctx)
    case abBinPred(abImply, p1, p2) => interp_abPred(p1, ctx) ==> interp_abPred(p2, ctx)
    case abBinPred(abEquiv, p1, p2) => interp_abPred(p1, ctx) <==> interp_abPred(p2, ctx)
    case abIntRelPred(abIntEq, e1, e2) => interp_abIntExpr(e1, ctx) == interp_abIntExpr(e2, ctx)
    case abIntRelPred(abIntNeq, e1, e2) => interp_abIntExpr(e1, ctx) != interp_abIntExpr(e2, ctx)
    case abIntRelPred(abIntLe, e1, e2) => interp_abIntExpr(e1, ctx) <= interp_abIntExpr(e2, ctx)
    case abIntRelPred(abIntLt, e1, e2) => interp_abIntExpr(e1, ctx) < interp_abIntExpr(e2, ctx)
    case abIntRelPred(abIntGe, e1, e2) => interp_abIntExpr(e1, ctx) >= interp_abIntExpr(e2, ctx)
    case abIntRelPred(abIntGt, e1, e2) => 
    interp_abIntExpr(e1, ctx) > interp_abIntExpr(e2, ctx)
    case abRealRelPred(abRealEq, e1, e2) => 
    interp_abRealExpr(e1, ctx) == interp_abRealExpr(e2, ctx)
    case abRealRelPred(abRealNeq, e1, e2) => 
    interp_abRealExpr(e1, ctx) != interp_abRealExpr(e2, ctx)
    case abRealRelPred(abRealLe, e1, e2) => 
    interp_abRealExpr(e1, ctx) <= interp_abRealExpr(e2, ctx)
    case abRealRelPred(abRealLt, e1, e2) => 
    interp_abRealExpr(e1, ctx) < interp_abRealExpr(e2, ctx)
    case abRealRelPred(abRealGe, e1, e2) => 
    interp_abRealExpr(e1, ctx) >= interp_abRealExpr(e2, ctx)
    case abRealRelPred(abRealGt, e1, e2) => 
    interp_abRealExpr(e1, ctx) > interp_abRealExpr(e2, ctx)
  }
}

/*****************
 * Compilation
 *****************/

/* compile event-b int expression to atelierb int expression */

function method compileIntBinOp(op: ebIntBinOp) : abIntBinOp
{
  match op {
    case ebIntAdd => abIntAdd
    case ebIntSub => abIntSub
    case ebIntMul => abIntMul
    case ebIntDiv => abIntDiv
  }
}

function method compileIntExpr(e: ebIntExpr) : abIntExpr
  decreases e
  ensures forall ctx: Context ::
            interp_abIntExpr(compileIntExpr(e), ctx) ==
            interp_ebIntExpr(e, ctx)
{
  match e {
    case ebIntLitExpr(n) => abIntLitExpr(n)
    case ebIntIdExpr(v) => abIntIdExpr(v)
    case ebIntUnExpr(ebNeg, e) => abIntUnExpr(abIntNeg, compileIntExpr(e))
    case ebIntBinExpr(op, e1, e2) => abIntBinExpr(compileIntBinOp(op), compileIntExpr(e1), compileIntExpr(e2))
  }
}

/* compile event-b real expression to atelierb real expression */

function method compileRealBinOp(op: ebRealBinOp) : abRealBinOp
{
  match op {
    case ebRealAdd => abRealAdd
    case ebRealSub => abRealSub
    case ebRealMul => abRealMul
    case ebRealDiv => abRealDiv
  }
}

function method compileRealExpr(e: ebRealExpr) : abRealExpr
  decreases e
  ensures forall ctx: Context ::
            interp_abRealExpr(compileRealExpr(e), ctx) ==
            interp_ebRealExpr(e, ctx)
{
  match e {
    case ebRealLitZero => abRealLitZero
    case ebRealLitOne => abRealLitOne 
    case ebRealIdExpr(v) => abRealIdExpr(v)
    case ebRealUnExpr(ebNeg, e) => abRealUnExpr(abRealNeg, compileRealExpr(e))
    case ebRealBinExpr(op, e1, e2) => abRealBinExpr(compileRealBinOp(op), compileRealExpr(e1), compileRealExpr(e2))
  }
}


function method compileBinPredOp(op: ebBinPredOp) : abBinPredOp
{
  match op {
    case ebLand => abLand
    case ebLor => abLor
    case ebImply => abImply
    case ebEquiv => abEquiv
  }
}

function method compileIntRelOp(op: ebIntRelOp) : abIntRelOp
{
  match op {
    case ebIntEq => abIntEq
    case ebIntNeq => abIntNeq
    case ebIntLe => abIntLe
    case ebIntLt => abIntLt
    case ebIntGe => abIntGe
    case ebIntGt => abIntGt
  }
}

function method compileRealRelOp(op: ebRealRelOp) : abRealRelOp
{
  match op {
    case ebRealEq => abRealEq
    case ebRealNeq => abRealNeq
    case ebRealLe => abRealLe
    case ebRealLt => abRealLt
    case ebRealGe => abRealGe
    case ebRealGt => abRealGt
  }
}

function method compilePred(p: ebPred) : abPred
  decreases p
  ensures forall ctx: Context ::
            interp_abPred(compilePred(p), ctx) ==
            interp_ebPred(p, ctx)
{
  match p {
    case ebLitPred(ebTrue) => abLitPred(abTrue)
    case ebLitPred(ebFalse) => abLitPred(abFalse)
    case ebUnPred(ebNot, p) => abUnPred(abNot, compilePred(p))
    case ebBinPred(op, p1, p2) => abBinPred(compileBinPredOp(op), compilePred(p1), compilePred(p2))
    case ebIntRelPred(op, e1, e2) => abIntRelPred(compileIntRelOp(op), compileIntExpr(e1), compileIntExpr(e2))
    case ebRealRelPred(op, e1, e2) => abRealRelPred(compileRealRelOp(op), compileRealExpr(e1), compileRealExpr(e2))
  }
}
